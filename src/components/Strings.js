export const Albums = 'Albums';
export const Photos = 'Fotos';
export const Comments = 'Comentarios';
export const See = 'Ver';
export const SeePhoto = 'Ver foto';
export const SeeComments = 'Ver comentarios';
