import React, { Component } from 'react';
import { ScrollView, Text, View, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import PhotoCommentDetail from './PhotoCommentDetail';

class PhotoComments extends Component {
  state = { comments: null };

  componentWillMount() {
    /*axios.get(`https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=6e8a597cb502b7b95dbd46a46e25db8d&photoset_id=${this.props.albumId}&user_id=137290658%40N08&format=json&nojsoncallback=1`)
      .then(response => this.setState({ photos: response.data.photoset.photo }));*/
    axios.get(`https://api.flickr.com/services/rest/?method=flickr.photos.comments.getList&api_key=6e8a597cb502b7b95dbd46a46e25db8d&photo_id=${this.props.photoId}&format=json&nojsoncallback=1`)
      .then(response => this.setState({ comments: response.data.comments.comment }))
      .catch(function(error) {
        console.log('No se pudieron cargar los comentarios: ' + error.message);
        Alert.alert(
          'Error de conexión',
          'No se pudieron cargar los comentarios: ' + error.message,
          [
            {text: 'Volver', onPress: () => Actions.pop()},
          ],
          { cancelable: false }
        )
      });

  }

  renderComments() {
    return this.state.comments.map(comment =>
      <PhotoCommentDetail id = {comment.id} author = {comment.author} authorname = {comment.authorname} commentText = {comment._content} />
    );
  }

  render() {
    console.log(this.state);


    if (!this.state.comments) { 
      return (
        <View style={{ flex: 1 }}>
          <Text>
              Loading...
          </Text>
        </View>
      );  
    }

    return (
        <View style={{ flex: 1 }}>
            <ScrollView>
                {this.renderComments()}
            </ScrollView>
        </View>
    );
  }
}

export default PhotoComments;